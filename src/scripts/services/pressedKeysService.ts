export const pressedKeys = new Set<string>();

interface IRules {
    [key : string] : () => void;
}

export class PressedKeysService {
  private rules : IRules;

  constructor(rules : IRules) {
    this.rules = rules;

    document.body.onkeydown = (ev) => {
      const { code } = ev;
      if (!pressedKeys.has(code)) {
        const prevPressedKeys = new Set(pressedKeys);
        pressedKeys.add(code);
        this.checkRules(prevPressedKeys);
      }
    };

    document.body.onkeyup = (ev) => {
      const { code } = ev;
      pressedKeys.delete(code);
    };
  }

  checkRules(prevPressedKeys : Set<string>) : void {
    Object.entries(this.rules).forEach(([key, value]) => {
      const combination = key.split(',');
      const isCombPressed = combination.every(part => pressedKeys.has(part));
      const isCombWasPressed = combination.every(part => prevPressedKeys.has(part));
      if (isCombPressed && !isCombWasPressed) {
        value();
      }
    });
  }
}
