import { createElement } from '../helpers/domHelper';
import { capitalizeFirstLetter } from '../helpers/strHelper';
import IFighter, { Position } from '../interfaces/fighter';

export function createFighterImage(fighter : IFighter) : HTMLElement {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes
  });

  return imgElement;
}

function appendInfoToElement(element : HTMLElement, obj : IFighter) : void {
  const keys = Object.keys(obj).filter((key) => key !== '_id' && key !== 'source');
  keys.forEach((key) => {
    const span = createElement({ tagName: 'span' });
    const strong = createElement({ tagName: 'strong' });
    const p = createElement({ tagName: 'p' });
    strong.innerText = `${capitalizeFirstLetter(key)}:`;
    p.innerText = (<string | number>(obj[(key as keyof IFighter)])).toString();
    span.appendChild(strong);
    span.appendChild(p);
    element.appendChild(span);
  });
}

function createFighterInfo(fighter : IFighter) : HTMLElement {
  const infoElement = createElement({
    tagName: 'div',
    className: 'fighter___info'
  });

  appendInfoToElement(infoElement, fighter);

  return infoElement;
}

export function createFighterPreview(fighter : IFighter, position : Position) : HTMLElement {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`
  });

  if (fighter) {
    const fighterImage = createFighterImage(fighter);
    const fighterInfo = createFighterInfo(fighter);
    fighterImage.classList.add(positionClassName);
    fighterElement.appendChild(fighterImage);
    fighterElement.appendChild(fighterInfo);
  }

  return fighterElement;
}
