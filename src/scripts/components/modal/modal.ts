import { createElement } from '../../helpers/domHelper';

type CloseFunc = () => void;

interface IModalData {
  title: string;
  bodyElement: HTMLElement;
  onClose: CloseFunc;
}

function getModalContainer() {
  return document.getElementById('root');
}

function hideModal() {
  const modal = document.getElementsByClassName('modal-layer')[0];
  if (modal) {
    modal.remove();
  }
}

function createHeader(title : string, onClose : CloseFunc) {
  const headerElement = createElement({ tagName: 'div', className: 'modal-header' });
  const titleElement = createElement({ tagName: 'span' });
  const closeButton = createElement({ tagName: 'div', className: 'close-btn' });

  titleElement.innerText = title;
  closeButton.innerText = '×';

  const close = () => {
    hideModal();
    onClose();
  };
  closeButton.addEventListener('click', close);
  headerElement.append(titleElement, closeButton);

  return headerElement;
}

function createModal({ title, bodyElement, onClose } : IModalData) {
  const layer = createElement({ tagName: 'div', className: 'modal-layer' });
  const modalContainer = createElement({ tagName: 'div', className: 'modal-root' });
  const header = createHeader(title, onClose);

  modalContainer.append(header, bodyElement);
  layer.append(modalContainer);

  return layer;
}

export function showModal({ title, bodyElement, onClose } : IModalData) : void {
  const root = <HTMLElement>getModalContainer();
  const modal = createModal({ title, bodyElement, onClose });

  root.append(modal);
}
