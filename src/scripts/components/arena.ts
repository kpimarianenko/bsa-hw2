import { createElement } from '../helpers/domHelper';
import { createFighterImage } from './fighterPreview';
import { showWinnerModal } from './modal/winner';
import { fight } from './fight';
import IFighter, { Position } from '../interfaces/fighter';

function createHealthIndicator(fighter : IFighter, position : Position) {
  const { name } = fighter;
  const container = createElement({ tagName: 'div', className: 'arena___fighter-indicator' });
  const fighterName = createElement({ tagName: 'span', className: 'arena___fighter-name' });
  const indicator = createElement({ tagName: 'div', className: 'arena___health-indicator' });
  const bar = createElement({
    tagName: 'div',
    className: 'arena___health-bar',
    attributes: { id: `${position}-fighter-indicator` }
  });

  fighterName.innerText = name;
  indicator.append(bar);
  container.append(fighterName, indicator);

  return container;
}

function createHealthIndicators(leftFighter : IFighter, rightFighter : IFighter) {
  const healthIndicators = createElement({ tagName: 'div', className: 'arena___fight-status' });
  const versusSign = createElement({ tagName: 'div', className: 'arena___versus-sign' });
  const leftFighterIndicator = createHealthIndicator(leftFighter, 'left');
  const rightFighterIndicator = createHealthIndicator(rightFighter, 'right');

  healthIndicators.append(leftFighterIndicator, versusSign, rightFighterIndicator);
  return healthIndicators;
}

function createFighter(fighter : IFighter, position : Position) {
  const imgElement = createFighterImage(fighter);
  const positionClassName = position === 'right' ? 'arena___right-fighter' : 'arena___left-fighter';
  const fighterElement = createElement({
    tagName: 'div',
    className: `arena___fighter ${positionClassName}`
  });

  fighterElement.append(imgElement);
  return fighterElement;
}

function createFighters(firstFighter : IFighter, secondFighter : IFighter) {
  const battleField = createElement({ tagName: 'div', className: 'arena___battlefield' });
  const firstFighterElement = createFighter(firstFighter, 'left');
  const secondFighterElement = createFighter(secondFighter, 'right');

  battleField.append(firstFighterElement, secondFighterElement);
  return battleField;
}

function createArena(selectedFighters : IFighter[]) {
  const arena = createElement({ tagName: 'div', className: 'arena___root' });
  const healthIndicators = createHealthIndicators(selectedFighters[0], selectedFighters[1]);
  const fighters = createFighters(selectedFighters[0], selectedFighters[1]);
  arena.append(healthIndicators, fighters);
  return arena;
}

export function renderArena(selectedFighters : IFighter[]) : void {
  const root = <HTMLElement>document.getElementById('root');
  const arena = createArena(selectedFighters);

  root.innerHTML = '';
  root.append(arena);

  fight(selectedFighters[0], selectedFighters[1])
    .then((fighter) => showWinnerModal(fighter));
}
