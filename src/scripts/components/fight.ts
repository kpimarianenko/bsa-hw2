import { controls } from '../../constants/controls';
import Random from '../helpers/random';
import IFighter, { Position, IFighterState } from '../interfaces/fighter';
import { pressedKeys, PressedKeysService } from '../services/pressedKeysService';

const criticalHitCooldown = 10000;
type Resolve = (value: IFighter | PromiseLike<IFighter>) => void;

function getStartFighterState(fighter : IFighter, position : Position) : IFighterState {
  const { health } = fighter;
  return {
    health: health || 0,
    position,
    canCrit: true,
    isBlocked: () => pressedKeys.has(position === 'left' ? controls.PlayerOneBlock : controls.PlayerTwoBlock)
  };
}

function setFightersHealthbar(curHealth : number, maxHealth : number, position : Position) {
  const id = `${position}-fighter-indicator`;
  const indicator = <HTMLElement>document.getElementById(id);
  indicator.style.width = `${(curHealth / maxHealth) * 100}%`;
}

function setFightersHealthbars(firstFighter : IFighter, secondFighter : IFighter) {
  if (firstFighter && firstFighter.state && firstFighter.health) {
    setFightersHealthbar(
      firstFighter.state.health,
      firstFighter.health,
      firstFighter.state.position
    );
  }
  if (secondFighter && secondFighter.state && secondFighter.health) {
    setFightersHealthbar(
      secondFighter.state.health,
      secondFighter.health,
      secondFighter.state.position
    );
  }
}

export function getHitPower(fighter : IFighter) : number {
  const { attack: damage } = fighter;
  if (!damage) return 0;
  const criticalHitChance = Random.getRandomFloat(1, 2);
  const power = damage * criticalHitChance;
  return power;
}

export function getBlockPower(fighter : IFighter) : number {
  const { defense } = fighter;
  if (!defense) return 0;
  const dodgeChance = Random.getRandomFloat(1, 2);
  const power = defense * dodgeChance;
  return power;
}

export function getDamage(attacker : IFighter, defender : IFighter) : number {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return Math.max(damage, 0);
}

function critAttack(attacker : IFighter, defender : IFighter, resolve : Resolve) : void {
  if (!attacker.state || !defender.state || !attacker.state.canCrit || !attacker.attack) return;
  attacker.state.canCrit = false;
  setTimeout(() => { (<IFighterState>attacker.state).canCrit = true; }, criticalHitCooldown);
  const { state } = defender;
  state.health -= attacker.attack * 2;
  if (state.health <= 0) {
    state.health = 0;
    resolve(attacker);
  }
  setFightersHealthbars(attacker, defender);
}

function attack(attacker : IFighter, defender : IFighter, resolve : Resolve) : void {
  if (!attacker.state || !defender.state || attacker.state.isBlocked()) return;
  const { state } = defender;
  state.health -= state.isBlocked() ? 0 : getDamage(attacker, defender);
  if (state.health <= 0) {
    state.health = 0;
    resolve(attacker);
  }
  setFightersHealthbars(attacker, defender);
}

export async function fight(firstFighter : IFighter, secondFighter : IFighter) : Promise<IFighter> {
  return new Promise((resolve) => {
    firstFighter.state = getStartFighterState(firstFighter, 'left');
    secondFighter.state = getStartFighterState(secondFighter, 'right');

    new PressedKeysService({
      [controls.PlayerOneAttack]: () => attack(firstFighter, secondFighter, resolve),
      [controls.PlayerTwoAttack]: () => attack(secondFighter, firstFighter, resolve),
      [controls.PlayerOneCriticalHitCombination.toString()]: () => {
        critAttack(firstFighter, secondFighter, resolve);
      },
      [controls.PlayerTwoCriticalHitCombination.toString()]: () => {
        critAttack(secondFighter, firstFighter, resolve);
      }
    });
  });
}
