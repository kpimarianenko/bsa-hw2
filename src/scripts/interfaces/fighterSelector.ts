interface FighterSelector {
    (event : Event, id : string): void;
}

export default FighterSelector;
