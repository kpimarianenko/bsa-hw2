interface IAttribute {
  [key: string]: string;
}

interface IHTMLAttributes {
  tagName : string;
  className? : string;
  attributes? : IAttribute;
}

export default IHTMLAttributes;
