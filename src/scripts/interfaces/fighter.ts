export type Position = 'left' | 'right';

export interface IFighterState {
    health: number;
    position: Position;
    canCrit: boolean;
    isBlocked: () => boolean
}

interface IFighter {
    _id : string;
    source : string;
    name : string;
    health? : number;
    attack? : number;
    defense? : number;
    state?: IFighterState;
}

export default IFighter;
